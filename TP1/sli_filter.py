#!/bin/python3

import numpy as np

def valconv(h, u, n):
    a=len(h) # pour un tableau numpy len(v)==v.shape[0]
    b=len(u)
    u=u.reshape(-1) #s’assurer que u et v son de la meme forme
    h=h.reshape(-1) #

    out = np.zeros(a+b-1)
    
    idx = np.arange(max(n - (b-1), 0), min(a, n+1)) # Formule trouvée précédemment
    
    return (h[idx]*u[n-idx]).sum()

print(valconv(np.ones(3),np.ones(2),0)) # doit renvoyer 1
print(valconv(np.ones(3),np.ones(2),2)) # doit renvoyer 2